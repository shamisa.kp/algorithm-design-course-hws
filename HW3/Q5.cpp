#include <iostream>
#include <utility>
#include <algorithm>

typedef long long int LLI;

using namespace std;

int main() {
    int n{0};
    cin >> n;

    int* k{new int[n]};
    for (int i{0}; i < n; ++i) {
        cin >> k[i];
    }

    pair<int, int> **p{new pair<int, int>*[n]};
    pair<int, int> **w{new pair<int, int>*[n]};

    int cnt{0};
    for (int j{0}; j < n; ++j) {
        p[j] = new pair<int, int>[k[j]];
        for (int i{0}; i < k[j]; ++i) {
            cin >>p[j][i].first;
            p[j][i].second = cnt;
            cnt++;
        }
    }
    cnt = 0;

    for (int j{0}; j < n; ++j) {
        w[j] = new pair<int, int>[k[j]];
        for (int i{0}; i < k[j]; ++i) {
            cin >>w[j][i].first;
            w[j][i].second = cnt;
            cnt++;
        }
    }

    pair<double , int> proportion[n];
    for(int i{0}; i < n; i++) {
        int p_t{0};
        int w_t{0};
        for (int j{0}; j < k[i] ; ++j) {
            p_t += p[i][j].first;
            w_t += w[i][j].first;
        }
        proportion[i].first = (static_cast<double>(p_t)/ static_cast<double>(w_t));
        proportion[i].second = i;
    }

    sort(proportion, proportion + n);

    pair<double, int> **each{new pair<double, int>*[n]};
    for (int l{0}; l < n; ++l) {
        double temp{0};
        each[l] = new pair<double, int>[k[l]];
        for (int i{0}; i < k[l]; ++i) {
            temp = (static_cast<double>(p[l][i].first) / static_cast<double>(w[l][i].first));
            each[l][i].first = temp;
            each[l][i].second = p[l][i].second;
        }
        sort(each[l], each[l]+ k[l]);
    }

    LLI cost{0};
    LLI total_p{0};
    for (int m{0}; m < n; ++m) {
        int class_num{proportion[m].second};
        int index {p[class_num][0].second};
        for (int i{0}; i < k[class_num]; ++i) {
            total_p += p[class_num][each[class_num][i].second-index].first;
            cost += w[class_num][each[class_num][i].second-index].first * total_p;
        }
    }

    cout << cost << endl;

    for (int j{0}; j < n; ++j) {
        int class_num{proportion[j].second};
        for (int i{0}; i < k[class_num]; ++i) {
            cout << each[class_num][i].second + 1 << " ";
        }
    }
    return 0;
}