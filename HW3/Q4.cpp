#include <iostream>
#include <list>
#include <queue>

using namespace std;


int main(){
    int n, m, k;
    cin >> n >> m >> k;

    int V{n + 1};
    int* distance{new int[V]};
    list<int> *adj{new list<int>[V]};

    int input{0};
    for (int i{0}; i < k; ++i) {
        cin >> input;
        adj[0].push_back(input);
    }

    int u{0};
    for (int j{0}; j < m ; ++j) {
        cin >> input >> u;
        adj[input].push_back(u);
        adj[u].push_back(input);
    }

    /* BFS */

    bool* visited{new bool[V]};
    for (int l{0}; l < V ; ++l) {
        visited[l] = false;
    }

    queue<int> queue;
    visited[0] = true;
    distance[0] = 0;
    queue.push(0);

    list<int>::iterator itr;

    while(!queue.empty()) {
        int source{queue.front()};
        queue.pop();
        for (itr = adj[source].begin(); itr != adj[source].end(); itr++) {
            if(!visited[*itr]) {
                visited[*itr] = true;
                queue.push(*itr);
                distance[*itr] = distance[source] + 1;
            }
        }
    }

    for (int j{1}; j < V; ++j) {
        cout << distance[j] - 1 << endl;
    }

    delete[] distance;

    return 0;
}