#include <iostream>

typedef long long int LLI;

LLI divideInv(LLI* n, int begin, int end);
LLI mergeInv(LLI* n, int begin, int m, int end);

int main() {
    int n{};
    LLI* numbers{nullptr};
    LLI cnt{0};

    std::cin >> n;
    numbers = new LLI[n];
    for(int i{0}; i < n; i++) {
        std::cin >> numbers[i];
    }

    cnt = divideInv(numbers, 0, n - 1);
    cnt = cnt % 1000000007;
    std::cout << cnt << std::endl;
    delete[] numbers;
    return 0;
}

LLI divideInv(LLI* n, int begin, int end) {
    int m{0};
    LLI s1, s2, s3;
    if(begin >= end) {
        return 0;
    }
        m = (begin + end) / 2;
        s1 = divideInv(n, begin, m);
        s2 = divideInv(n, m+1, end);
        s3 = mergeInv(n, begin, m, end);
        return s1 + s2 + s3;
}

LLI mergeInv(LLI* n, int begin, int m, int end) {
    LLI * newarr {new LLI[end - begin + 1]};
    int i = begin;
    int j = m + 1;
    int k = 0;
    LLI counter{0};

    while (i <= m && j <= end) {
        if (n[i] < n[j]) {
            newarr[k++] = n[i++];
            counter += j - m - 1;
        }
        else
            newarr[k++] = n[j++];
    }

    while(i <= m){
        newarr[k++] = n[i++];
        counter += end - m;
    }

    while(j <= end) {
        newarr[k++] = n[j++];
    }

    for(int f{0}; f <= end - begin; f++){
        n[begin + f] = newarr[f];
    }
    return counter;
}