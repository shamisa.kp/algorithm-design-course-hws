#include<iostream>
#include<string>
#include <queue>
#include <vector>
#include <cmath>
#include <list>
#include <algorithm>


using namespace std;
typedef long long int LLI;

bool myCompare(pair<string, int> a, pair<string, int> b) {
    return a.first < b.first;
}

class Graph
{
    int V;

    list<int> *adj;
public:
    explicit Graph(int V);

    void addEdge(int v, int w);

    void BFS(int s, int* p);
};

Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w);
    adj[w].push_back(v);
}

void Graph::BFS(int s, int* p) {

    auto visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;

    list<int> queue;

    visited[s] = true;
    queue.push_back(s);

    list<int>::iterator i;

    while (!queue.empty()) {

        s = queue.front();
//        cout << s << " ";
        queue.pop_front();

        for (i = adj[s].begin(); i != adj[s].end(); ++i) {
            if (!visited[*i]) {
                p[*i] = s;
                visited[*i] = true;
                queue.push_back(*i);
            }
        }
    }
}
bool isEdge(string& aa, string& bb) {
    string a = aa;
    string b = bb;
    transform(a.begin(), a.end(), a.begin(), ::tolower);
    transform(b.begin(), b.end(), b.begin(), ::tolower);

    if(a.length() == b.length()) {

        int n{ 0 };
        for (unsigned long i{ 0 }; i < a.length(); i++) {
            if (a[i] == b[i]) continue;
            n++;
            if (n > 1) return false;
        }

        return n == 1;

    }
    else if(a.length() == b.length() + 1 || b.length() == a.length() + 1) {
        int i{0};
        int j{0};
        int n{0};
        if(a.length() > b.length()) {
            while (i < a.length()) {
                if(a[i] != b[j]) {
                    i++;
                    n++;
                    if(n > 1){
                        return false;
                    }
                } else{
                    i++;
                    j++;
                }
            }
            return n == 1;

        }
        else {
            while (j < b.length()) {
                if(a[i] != b[j]) {
                    j++;
                    n++;
                    if(n > 1){
                        return false;
                    }
                } else{
                    i++;
                    j++;
                }
            }
            return n == 1;

        }

    }
    else
        return false;

}

int main() {

    int k{ 0 };
    LLI q{ 0 };

    cin >> k >> q;

    vector <pair<string, int>> input;
    Graph g(k);

    string temp;
    for (int i{ 0 }; i < k; i++) {
        cin >> temp;
        input.emplace_back(pair<string, int>(temp, i));
    }

    pair<string, string> query[q];

    for (LLI j{ 0 }; j < q; j++) {
        cin >> query[j].first;
        cin >> query[j].second;
    }
    for (int l = 0; l < k; ++l) {
        for (int i = l + 1; i < k; ++i) {
            if(isEdge(input[l].first, input[i].first)){
                g.addEdge(input[l].second, input[i].second);
            }
        }
    }

    int** parent{new int*[k]};
    for (int m = 0; m < k; ++m) {
        parent[m] = new int[k];
        for(int i = 0; i < k; ++i) {
            parent[m][i] = -1;
        }
    }
    int n{0};

    while(n < k){
        g.BFS(n, parent[n]);
        n++;
    }

    vector <pair<string, int>> t_input;
    for (int f = 0; f < k; ++f) {
        t_input.push_back(input[f]);
        //cout << input[f].first << " " << input[f].second << endl;
    }
    sort(t_input.begin(), t_input.end(), myCompare);

    for (int i = 0; i < q; ++i) {
        string start{query[i].first};
        string end{query[i].second};

        auto l = lower_bound(t_input.begin(), t_input.end(), pair<string, int>(start, 0), myCompare);
        if((*l).first == start) {
            int s{(*l).second};
            auto m = lower_bound(t_input.begin(), t_input.end(), pair<string, int>(end, 0), myCompare);
            if((*m).first == end) {
                int e{(*m).second};
                vector<int> final;
                int p{e};
                final.push_back(p);
                while(p != s){
                    final.push_back(parent[s][p]);
                    p = parent[s][p];
                }
//                final.push_back(s);
                for(int j = final.size() - 1; j >= 0; j--) {
                      cout << input[final[j]].first << " ";
//                    cout << final[j] << " ";

                }
                cout << endl;
            }
            else cout << "*" << endl;
        }
        else {
            cout << "*" << endl;
        }

    }
    return 0;
}
