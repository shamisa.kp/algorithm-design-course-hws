#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

struct row {
    int r;
    int start;
    int end;

};

struct column {
    int c;
    int start;
    int end;
};

bool bpm(bool** arr, int u,
         bool seen[], int matchR[], int n, int m)
{
    for (int v = 0; v < m; v++)
    {

        if (arr[u][v] && !seen[v])
        {
            seen[v] = true;

            if (matchR[v] < 0 || bpm(arr, matchR[v],
                                     seen, matchR, n, m))
            {
                matchR[v] = u;
                return true;
            }
        }
    }
    return false;
}

int MBM(bool** arr, int n, int m) {
        int matchR[m];

        memset(matchR, -1, sizeof(matchR));

        int result = 0;
        for (int u = 0; u < n; u++)
        {

            bool seen[m];
            memset(seen, 0, sizeof(seen));

            if (bpm(arr, u, seen, matchR, n, m))
                result++;
        }
        return result;
}

void makeRow(char** arr, int n, int m,  vector<row>& vR) {

    for (int i = 0; i < n; ++i) {
        int j{0};
        while (j < m) {
            int s{j};
            while (arr[i][j] == '.') {
                j++;
                if(j == m)
                    break;
            }
            if(j != s) {
                row r{i, s, j - 1};
                vR.push_back(r);
            }
            j++;
        }
    }
}
void makeColumn(char** arr, int n, int m,  vector<column>& vC) {
    for (int i = 0; i < m; ++i) {
        int j{0};
        while (j < n) {
            int s{j};
            while (arr[j][i] == '.') {
                j++;
                if(j == n)
                    break;
            }
            if(j != s) {
                column c{i, s, j - 1};
                vC.push_back(c);
            }
            j++;
        }
    }
}

void generateEdge(vector<row>& vR, vector<column>& vC, bool** arr) {
    for (int i = 0; i < vR.size(); ++i) {
        for (int j = 0; j < vC.size(); ++j) {
            if(vC[j].c >= vR[i].start && vC[j].c <= vR[i].end) {
                if(vR[i].r >= vC[j].start && vR[i].r <= vC[j].end)
                    arr[i][j] = true;
            }
            else
                arr[i][j] = false;
        }
    }
}





int main() {

    int n{0};
    int m{0};

    cin >> n >> m;

    vector<column> vC;
    vector<row> vR;

    char** arrInput{new char*[n]};

    for (int i{0}; i < n; ++i) {
        arrInput[i] = new char[m];
        for (int j{0}; j < m; ++j) {
            cin >> arrInput[i][j];
        }
    }

    makeRow(arrInput, n, m, vR);
    makeColumn(arrInput, n, m, vC);

    bool** mbmArr{new bool*[vR.size()]};

    for (int k = 0; k < vR.size(); ++k) {
        mbmArr[k] = new bool[vC.size()];
    }

    generateEdge(vR, vC, mbmArr);
    cout << MBM(mbmArr, vR.size(), vC.size()) << endl;


    return 0;
}
