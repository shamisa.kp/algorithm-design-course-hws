#include <iostream>
#include <algorithm>

using namespace std;

typedef long int LLI;

LLI TAsort(LLI** dparr, LLI* arr, LLI l, LLI r);

int main() {
    LLI n, q{0};

    cin >> n >> q;
    auto dpArr = new LLI*[n];
    for(LLI i{0}; i < n; i++){
        dpArr[i] = new LLI[n + 1];
    }

    for(LLI k{0}; k < n; k++) {
        for(LLI y{0}; y < n + 1; y++) {
            dpArr[k][y] = -1;
        }
    }



    LLI* arr{new LLI[n]};

    for(LLI i{0}; i < n; i++) {
        cin >> arr[i];
    }

    for(LLI j{0}; j < q; j++) {
        LLI l, r{0};
        cin >> l >> r;
        cout << TAsort(dpArr, arr, l - 1, r - 1) << endl;
    }

    for (LLI m{0}; m < n; ++m) {
        delete[] dpArr[m];
    }

    delete[] dpArr;
    return 0;
}

LLI TAsort(LLI** dparr, LLI* arr, LLI l, LLI r) {

    if(dparr[l][r] != -1)
        return dparr[l][r];

    if(is_sorted(arr + l, arr + r))
        return dparr[l][r] = 1;

    LLI mid = (l + r) / 2;

    return dparr[l][r] = TAsort(dparr, arr, l, mid) + TAsort(dparr, arr, mid, r) + 1;

}


